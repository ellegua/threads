package com.company;

import java.util.Scanner;

import static java.lang.Thread.sleep;

public class Main {

    public static void main(String[] args) {
        //System.out.println("Main is running.");
        Scanner in = new Scanner(System.in);
        Timer timer = new Timer();
        Prompt prompt = new Prompt();
        prompt.start();
        timer.start();

        while (timer.isAlive()) {
            if (!prompt.isAlive()) {
                timer.interrupt();
                //System.out.println(prompt.getClass().getSimpleName() + " wins!");
                break;
            }
        }
        if (prompt.isAlive()) {
            prompt.interrupt();
            //System.out.println(timer.getClass().getSimpleName() + " wins!");
            System.out.println("Time is over!");
        }
        //System.out.println("Main is finished.");
    }
}
