package com.company;

import java.util.Scanner;

public class Timer extends Thread {

    @Override
    public void run() {
        //System.out.println(this.getClass().getSimpleName() + " is running.");

        for (int i = 5; i > 0; i--) {//hard-code
            if (!isInterrupted()) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    //System.out.println(this.getClass().getSimpleName() + " is interrupted.");
                    break;
                }
                System.out.println(i);
            } else break;
        }
        //System.out.println(this.getClass().getSimpleName() + " is finished.");
    }
}

