package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Prompt extends Thread {


    public void run() {
        //System.out.println(this.getClass().getSimpleName() + " is running.");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String code;
        do {
            System.out.println("Prove that you are not a robot!");
            //System.out.println("Guess the riddle.");
            System.out.println("Answer. Who are you?");
            //System.out.println("Please type something: ");
            try {
                while (!bufferedReader.ready()) {
                    Thread.sleep(1000);
                }
                code = bufferedReader.readLine();
            } catch (InterruptedException e) {
                //e.printStackTrace();
                //System.out.println(this.getClass().getSimpleName() + " is interrupted.");
                code = " ";
            } catch (IOException e) {
                e.printStackTrace();
                code = " ";
            }
        } while ("".equals(code));
        if (code.equals("man") || code.equals("Man")) {//hard-code
            System.out.println("You are not a robot!");
        } else {
            System.out.println("Wrong answer!");
        }
        //System.out.println(this.getClass().getSimpleName() + " is finished.");
    }
}


